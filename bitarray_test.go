package bitarray

import (
	"math/rand"
	"testing"
)

func TestEachWordSize(t *testing.T) {
	numWords := uint(250000)
	valid := false

	for i := byte(1); i <= 8; i = i << 1 {
		bitArray := NewBitArray(numWords, i)

		valid =  bitArray != nil
		if ! valid {
					t.Errorf("Initilization broke on word size of %d", i)
		}
	}
}

func TestRandomNumWordsUInt32(t *testing.T) {
	numWords := uint(rand.Uint32())
	valid := false

	for i := byte(1); i <= 8; i = i << 1 {
		bitArray := NewBitArray(numWords, i)

		valid =  bitArray != nil
		if ! valid {
					t.Errorf("Initilization broke on word size of %d and %d",
								   i,
								 	 numWords)
		}
	}
}

func TestAllocLength(t *testing.T) {
	bitArray := NewBitArray(250000, 4)

	if bitArray.GetAllocLen() != len(bitArray.GetBytes()) {
		t.Error("Allocation length did not match.")
	}
}

func TestSetAndGetMatch(t *testing.T) {
	bitArray := NewBitArray(250000, 4)
	loc := uint(rand.Intn(1000))
	val := byte(0x04)

	bitArray.SetWord(loc, val)

	if bitArray.GetWord(loc) != val {
		t.Errorf("Setting binary value %b at location %d and retrieving it yielded %b.",
						 val,
					 	 loc,
					 	 bitArray.GetWord(loc))
	}
}
