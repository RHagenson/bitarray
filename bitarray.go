package bitarray

/*
Generalized BitArray with variable bit width
*/
type BitArray struct {
	b             []byte // Bit array stored in byte array
	wordSize byte   // Word size; 1, 2, 4, 8 are supported
	countPerByte  byte   // Number of words per byte
	byteCount     uint   // Total bytes needed
}

func NewBitArray(byteCount uint, wordSize byte) *BitArray {
	return new(BitArray).Init(byteCount, wordSize)
}

func (s *BitArray) Init(byteCount uint, wordSize byte) *BitArray {
	validBitLen := false
	for i := uint(0); i < 4; i++ {
		if wordSize == 0x08>>i {
			validBitLen = true
			s.countPerByte = 0x08 / wordSize
			break
		}
	}
	if !validBitLen {
		panic("Only wordSize values of 1, 2, 4, and 8 are supported.")
	}
	s.b = make([]byte, byteCount/uint(s.countPerByte)+1)
	s.byteCount = byteCount
	s.wordSize = wordSize
	return s
}

func (s *BitArray) GetAllocLen() int {
	return len(s.b)
}

func (s *BitArray) SetWord(pos uint, val byte) {
	whichByte := pos / uint(s.countPerByte)
	whichPos := pos % uint(s.countPerByte)
	n := byte(whichPos)
	w := s.wordSize
	onAtWord := (byte(0xFF<<(8-w)) >> (n * w)) ^ 0xFF
	zr := s.b[whichByte] & onAtWord
	sr := byte(val<<(8-w)) >> (n * w)
	s.b[whichByte] = zr | sr
}

func (s *BitArray) GetBytes() []byte {
	return s.b
}

func (s *BitArray) GetWord(pos uint) byte {
	whichByte := pos / uint(s.countPerByte)
	whichPos := pos % uint(s.countPerByte)
	n := byte(whichPos)
	w := s.wordSize
	onAtWord := (byte(0xFF<<(8-w)) >> (n * w)) // Turn on bits at word location
	word := s.b[whichByte] & onAtWord          // Extract word at location via AND

	return word >> (8 - (n+1)*w) // Offset word to lowest bits
}
